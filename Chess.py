def output(height, width):
    stars_space = ['*', '  ']
    result = ''
    for i in range(height):
        for j in range(width):
            if j % 2 == 0:
                result = result + stars_space[0]
            else:
                result = result + stars_space[1]
        print(result)
        stars_space = stars_space[::-1]
        result=''


def input_validation(num):
    try:
        num = int(num)
        return num
    except:
        pass


def main():
    while True:
        height = input_validation(input('Please enter height: '))
        width = input_validation(input('Please enter width: '))
        if height and width:
            break
        else:
            print('Enter only digits')

    output(height, width)


if __name__ == '__main__':
    main()
