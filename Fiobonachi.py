def fib(fib_position):
    if fib_position == 1 or fib_position == 2:
        return 1
    else:
        return fib(fib_position - 1) + fib(fib_position - 2)


def first_method(num):
    fibonachi_call_position = 1
    result = []
    while True:
        result.append(fib(fibonachi_call_position))
        fibonachi_call_position += 1
        if len(result) == num:
            break
    return result


def second_method(min, max):
    fibonachi_call_position = 1
    b = 0
    result = []
    while True:
        b = fib(fibonachi_call_position)
        fibonachi_call_position += 1
        if fib(fibonachi_call_position) >= min and fib(fibonachi_call_position) <= max:
            result.append(fib(fibonachi_call_position))
        elif fib(fibonachi_call_position) > max:
            break
    return result


def input_validation(inputed_numder):
    try:
        inputed_numder = int(inputed_numder)
        return inputed_numder
    except:
        pass


def main():
    while True:
        try:
            choose = input_validation(input('Choose a way\n1:Lengthwise\n2:By boundary values\n3:Exit\n'))
            if choose == 1:
                try:
                    num = input_validation(input('Please input  len: '))
                    print(first_method(num))
                except:
                    print('Input only digits')
            elif choose == 2:
                try:
                    min, max = int(input('Please input min: ')), int(input('Please input max: '))
                    print(second_method(min, max))
                except:
                    print('Input only digits')
            elif choose == 3:
                break
            else:
                print('Enter 1 or 2')
        except:
            pass


if __name__ == '__main__':
    main()
