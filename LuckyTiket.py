def first_method(num):
    first_part, second_part = 0, 0
    for j in range(3):
        number_on_position = num % 10
        num = num // 10
        if j > 2:
            first_part += int(number_on_position)
        else:
            second_part += int(number_on_position)
    if second_part == first_part:
        return 1
    else:
        return 0


def second_method(num):
    even, odd = 0, 0
    for i in range(6):
        number_on_position = num % 10
        num = num // 10
        if number_on_position % 2 == 0:
            even += number_on_position
        else:
            odd += number_on_position
    if even == odd:
        return 1
    else:
        return 0

def validator():
    while True:
        try:
            min = int(input('Input start number witch 6 digits: '))
            if len(str(min)) != 6:
                print('Len must be 6')
                continue
            max = int(input('Input stop number witch 6 digits: '))
            if len(str(max)) != 6:
                print('Len must be 6')
                continue
        except:
            print('Input correct symbols')
            continue
        if min > max:
            print('First number must be lower then second')
        else:
            return min, max


def main():
    first, second = 0, 0
    min, max = validator()
    for i in range(min,max):
        first += int(first_method(i))
        second += int(second_method(i))
    print('First method result: ', first, '\nSecond method result: ', second)
    if first > second:
        print('First method is better')
    elif first < second:
        print('Second method is better')
    elif first == second:
        print('Methods are equal')


if __name__ == '__main__':
    main()