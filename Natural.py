def output(m, n):
    ccount = 0
    llist = []
    while True:
         if ccount*ccount > m:
             llist.append(ccount)
         if len(llist)>n:
             break
         ccount +=1
    print(llist)

def validator():
    while True:
        try:
           n = int(input('Enter n: '))
           m = int(input('Enter m: '))
           return m, n
        except:
            print('Enter valid digits')
def main():
    m,n = validator()
    output(m,n)

if __name__ == '__main__':
    main()